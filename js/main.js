'use strict';
let spyArray = [];
$.getJSON('data.json').success((data) => {
    spyArray = data;
    console.log('eka', spyArray);
}).then(() => {
    initCards();
    startLoading();
});

let parsedHtml;

const initCards = () => {
    parsedHtml = '';
    for (const htmlItem of spyArray) {
        parsedHtml +=
            `<div class="w3-card-12">
        <img src="${htmlItem.thumbnail}"></img>
         <p> ${htmlItem.title}</p> 
         <button class="openbutton" src="${htmlItem.image}" coordsX="${htmlItem.coordinates.lat}" coordsY="${htmlItem.coordinates.lng}" time="${htmlItem.time}" >open</button>
         </div>`;
    }
}

const startLoading = () => {
    let categoryArray = [];
    for (const htmlItem of spyArray) {
        if (!categoryArray.includes(htmlItem.category)) {
            categoryArray.push(htmlItem.category);
            console.log('loki kategoria', htmlItem.category);
        }
    }
    for (const item of categoryArray) {
        console.log('catogoria:', item)
        parsedHtml +=
            `<button class="categorybutton" categ="${item}">${item}</button>`;
    }

    parsedHtml += `<button id="categoryAll">All</button>`;

    document.getElementById('theDiv').innerHTML = parsedHtml;

    const cards = document.querySelectorAll('.openbutton');
    for (const card of cards) {
        card.addEventListener('click', (evt) => {
            console.log(evt.target);
            let eventti = evt.target;
            let linkki = eventti.getAttribute('src');
            let coordsX = eventti.getAttribute('coordsX');
            let coordsY = eventti.getAttribute('coordsY');
            let time = eventti.getAttribute('time');
            console.log('linkki ja coordinaatit', linkki, coordsX, coordsY, time);
            openModal(linkki, coordsX, coordsY, time);
            // TODO
            // print the title property of selected item to console
        });
    }

    const categoryButtons = document.querySelectorAll('.categorybutton');
    for (const button of categoryButtons) {
        button.addEventListener('click', (evt) => {
            let eventti = evt.target;
            let categ = eventti.getAttribute('categ');
            console.log('eventti', categ)
            sortCategories(categ);
        });
    }
    const categoryAllButton = document.querySelector('#categoryAll');
    categoryAllButton.addEventListener('click', (evt) =>{
        initCards();
        startLoading();
    });
}

// Get the modal
const modal = document.getElementById('myModal');

// Get the <span> element that closes the modal
const span = document.getElementsByClassName('close')[0];

// When the user clicks on the button, open the modal
const openModal = (kuva, coordsX, coordsY, time) => {
    modal.style.display = 'block';
    const parsedd = `<img src="${kuva}"><p>${time}</p><div id="map"></div></img>`;
    document.getElementById('modalContent').innerHTML = parsedd;
    initMap(coordsX, coordsY);
}

// When the user clicks on <span> (x), close the modal
span.onclick = () => {
    modal.style.display = 'none';
}

// When the user clicks anywhere outside of the modal,  close it
window.onclick = (event) => {
    if (event.target == modal) {
        modal.style.display = 'none';
    }
}

const initMap = (coordsX, coordsY) => {
    const x = parseInt(coordsX);
    const y = parseInt(coordsY);
    const uluru = {lat: x, lng: y};
    console.log(x, y);
    const map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: uluru,
    });
    const marker = new google.maps.Marker({
        position: uluru,
        map: map,
    });
}

const sortCategories = (category) => {
    parsedHtml = '';
    for (const htmlItem in spyArray) {
        if (spyArray[htmlItem].category == category) {
            parsedHtml +=
                `<div class="w3-card-12">
        <img src="${spyArray[htmlItem].thumbnail}"></img>
         <p> ${spyArray[htmlItem].title}</p> 
         <button class="openbutton" src="${spyArray[htmlItem].image}" coordsX="${spyArray[htmlItem].coordinates.lat}" coordsY="${spyArray[htmlItem].coordinates.lng}" time="${spyArray[htmlItem].time}" >open</button>
         </div>`;
        }
    }
    startLoading();
};
